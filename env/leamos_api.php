<?php 

// DATABASE
define('DATABASE_DRIVER', 'pdo_mysql');
define('DATABASE_HOST', 'localhost');
define('DATABASE_SLAVE_HOST', 'localhost');
define('DATABASE_PORT', '3306');
define('DATABASE_NAME', 'leamos');
define('DATABASE_USER', 'root');
define('DATABASE_PASSWORD', 'root');
define('DATABASE_PATH', 'null');

define('DOMAIN','leamos.vm');

// MAILER
define('MAILER_TRANSPORT', 'smtp');
define('MAILER_HOST', 'smtp.mailgun.org');
define('MAILER_PORT', 25);
define('MAILER_AUTH_MODE', 'login');
define('MAILER_USER', 'noreply@leamos.com');
define('MAILER_PASSWORD', 'hWJRbqvLdCNW');

//PROCESS PARAMETERS
class ProcessParameters {
	public static $NotificationMails = 
		array (
			"leamos.dev@grupovi-da.com",
		);
}

// APP SECRET
define('APP_SECRET', 'u7dMFnyn89Hs9orXqYzvRU5k2OzXc0TT');

// FACEBOOK
define('FACEBOOK_APP_ID',"1582376771985624");
define('FACEBOOK_ACCESS_TOKEN_URL',"https://graph.facebook.com/oauth/access_token");
define('FACEBOOK_GRAPHAPI_URL',"https://graph.facebook.com/me");
define('FACEBOOK_SECRET',"25617e18f089bdbfb85db756c233f740");

// GOOGLE
define('GOOGLE_ACCESS_TOKEN_URL',"https://accounts.google.com/o/oauth2/token");
define('GOOGLE_GRAPHAPI_URL',"https://www.googleapis.com/plus/v1/people/me/openIdConnect");
define('GOOGLE_SECRET',"hQuPEIey0ywi6_MZ7psLFBb_");

// API BL
define('BL_API_URL',"http://api.pre.bajalibros.grupovi-da.biz/v3");
define('BL_API_KEY',"faf49d30d1d1ce0aafdcff0133240a5adbe69cb4cb7e3d8ce0e85734dcc71552");

// API PAGOS
//define('API_PAGOS_URL', "http://pre.apipagos.grupovi-da.biz");
define('API_PAGOS_URL', "http://apipagos.vm");
define('API_PAGOS_KEY', "0ad066a5d29f3f2a2a1c7c17dd082a799d00dad49f80252639edefaa546bd145");
// La key que debe mandar el api de pagos para interactuar con el api de leamos
define('API_PAGOS_PUBLIC_KEY', "5fdc492f81f3ca41f32a567c1fc96e273753b4f4b4d917341a6bbb9d17ac5b8c");
define('API_PAGOS_AMOUNT', 25.99);

define('API_INTERTRON_KEY', "8e43b82296e3b5103a082fb6a05a0e9385a9e6ba745da11386f670af3272a554");

// API LOANS
define('LOANS_API_URL',"http://bidi.vm/v1.5");
define('LOANS_API_KEY',"c7ad599c1fb351b13d8cb5c89871e47436074684");
define('LOANS_API_SECRET',"c16fc02b2ce73057d10960087cd99cef");
