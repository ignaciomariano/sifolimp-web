exec { 'disable-swap':
  path    => '/sbin',
  command => 'swapoff -a',
  user    => 'root',
} ->
file { '/etc/resolv.conf' :
  content => 'nameserver 8.8.8.8',
} ->
exec { 'apt-get update':
  command => 'apt-get update',
  path    => '/usr/bin/',
  timeout => 0,
  tries   => 3,
}

package { ['python-software-properties']:
  ensure  => 'installed',
  require => Exec['apt-get update'],
}

file { '/home/vagrant/.bash_aliases':
  ensure => 'present',
  source => 'puppet:///modules/puphpet/dot/.bash_aliases',
}

package { ['build-essential', 'vim', 'curl', 'git', 'git-core', 'ant']:
  ensure  => 'installed',
  require => Exec['apt-get update'],
}

class { 'apache': }




file { '/etc/apache2/sites-enabled/000-default.conf':
  ensure => absent,
  notify => Service['apache2']
}

class {'apache::mod::rewrite': }

apache::vhost { 'sifolimp.dev':
  server_name   => 'sifolimp.dev',
  docroot       => '/var/www/web',
  port          => '8082',
}

class php {
  # Installs PHP and restarts Apache to load the module
  package { ['php54', 'php54-apc', 'php54-mod-php', 'php5-mysql', 'php5-cli', 'php5-curl', 'php5-intl', 'php5-mcrypt', 'apache2' ]:
    ensure  => installed,
    notify  => Service['apache2'],
    require => [File['servergrove.repo'], Package['mysql-client'], Package['apache2']],
  }
}

class mysql {
  # Instalación del servidor de MySQL
    package { 'mysql-server':
      ensure => installed,
      require => Exec["apt-get update"],
  }
}

#APACHE
#class apache {
  # Ensures Apache2 is installed
  #package { 'apache2':
    #name => 'apache2-mpm-prefork', # httpd if CentOS
    #ensure => installed,
  #}
#}

# Arrancar el servicio de MySQL
#service { "mysql-server":
    #ensure => running,
    #require => Package["mysql-server"], 
#}





#class webserver {
  # Setups the virtual host
  #file { '/etc/apache2/sites-enabled/vhost-sifo.conf':
    #source  => '../../conf/vhost-sifo.conf',
    #source  => '/home/ignacio/repo/sifolimp-yii/conf/vhost-sifo.conf',
    #notify  => Service['apache2'],
    #require => Package['apache2'],
  #}
#}

#php::ini { 'php':
  #value   => ['date.timezone = "Europe/Madrid"'],
  #target  => 'php.ini',
  #service => 'apache',
#}

#root_password => 'sifolimp',
#require       => Exec['apt-get update'],

/*
mysql::grant { 'sf2-vagrant':
  mysql_privileges     => 'ALL',
  mysql_db             => 'sifolimp',
  mysql_user           => 'sifolimp',
  mysql_password       => 'sifolimp',
  mysql_host           => 'localhost',
  mysql_grant_filepath => '/home/vagrant/puppet-mysql',
}
*/
class { 'apt':
  always_apt_update => true,
}
