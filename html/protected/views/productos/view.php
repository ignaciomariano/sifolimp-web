<?php
/*
  $this->widget('zii.widgets.CDetailView', array(
  'data' => $model,
  'attributes' => array(
  'id',
  'codigo',
  'descripcion',
  'marca',
  'modelo',
  'categoria',
  'embalaje',
  'nombre_foto',
  ),
  ));
 */
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 well well-lg campos-form">
            <h2>Producto ID <?php echo $model->id; ?></h2>
            <div class="imagen-producto">
                <img src="/images/<?php echo $model->nombre_foto; ?>" alt="nombre" />
            </div>
            <ul class="list-unstyled">
                <!--<li><span>Precio:</span><b><?php #echo $model->codigo;    ?></b></li>-->
                <li><span>Código:</span><?php echo $model->codigo; ?></li>
                <li><span>Marca:</span><?php echo $model->marca; ?></li>
                <li><span>Categoría:</span><?php echo $model->categoria->categoria; ?></li>                
                <li><span>Descripción:</span><?php echo $model->descripcion; ?></li>                
                <li><span>Modelo:</span><?php echo $model->modelo; ?></li>
                <li><span>Embalaje:</span><?php echo $model->embalaje; ?></li>
            </ul>   
            <div class="opciones-btn">
                <!--
                <a class="btn btn-danger" href="/productos/delete/id/<?php #echo $model->id;   ?>" role="button">
                    <i class="fa fa-trash-o"></i> 
                    Eliminar Producto
                </a>
                -->
                <a class="btn btn-default" href="/productos/admin" role="button">
                    Volver
                </a>
                <a class="btn btn-default" href="/productos/update/<?php echo $model->id; ?>" role="button">
                    <i class="fa fa-pencil"></i> 
                    Editar producto
                </a>
                <!-- en el placeholder de 'cargar-producto-vendedor' tinen que aparecer los campos que ya estan cargados-->
            </div>     
        </div>
    </div>
</div><!-- /container-->