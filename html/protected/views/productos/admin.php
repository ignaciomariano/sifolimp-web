<script>
    function precios(prod) {
        if (confirm('Seguro que desea cambiar el precio al producto?')) {
            console.log(prod.value);
            console.log($(prod).attr("id"));
            var id = $(prod).attr("id");
            var precio = prod.value;
            $.ajax({
                url: '/productos/UpdatePrecio',
                type: 'POST',
                data: {id: id, precio: precio},
                success: function(data) {
                    console.log("OK" + data);
                },
                error: function(err) {
                    console.log("NO" + err);

                }
            });
        } else {
            console.log("Cancelado");
            location.reload();
        }
    }

    function status(data) {
        var product_id = $(data).attr('product_id');
        var status = data.value;
        $.ajax({
            url: '/productos/status',
            type: 'POST',
            data: {product_id: product_id, status: status},
            success: function(data) {
                console.log(data);
                $("#modal-product-status").modal();
            },
            error: function(err) {
                console.log(data);
            }
        });
    }

</script>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#productos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container">
    <a href="/productos/create" >
        <div class="btn-login goto">crear nuevo producto
        </div>
    </a>
    <?php if ($model != null): ?>
        <table class="table table-striped">
            <tr>
                <td>Código</td>
                <td>Marca</td>
                <td>Categoría</td>
                <td>Descripción</td>
                <td>Modelo</td>
                <td>Embalaje</td>
                <td>Estado</td>
                <td>Foto</td>
                <td>Ver</td>
            </tr>
            <?php foreach ($model as $data): ?>
                <tr>
                    <td><?php echo $data->codigo; ?></td>
                    <td><?php echo $data->marca; ?></td>
                    <td><?php echo $data->categoria->categoria; ?></td>
                    <td><?php echo $data->descripcion; ?></td>
                    <td><?php echo $data->modelo; ?></td>
                    <td><?php echo $data->embalaje; ?></td>
                    <td>
                        <select class="select-options" onchange="status(this)" product_id="<?php echo $data->id; ?>">
                            <option value="0" <?php if ($data->status == 0) { ?> selected <?php } ?>>Deshabilitado</option>
                            <option value="1" <?php if ($data->status == 1) { ?> selected <?php } ?>>Habilitado</option>
                        </select>
                    </td>
                    <td class="list-image">
                        <img src="/images/<?php echo $data->nombre_foto; ?>" 
                             alt="producto" />
                    </td>
                    <td>
                        <a href="/productos/<?php echo $data->id; ?>" class="editbtn">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>

    <nav>
        <ul class="pagination">
            <li>
                <a href="/productos/admin?page=<?php echo 0; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <?php for ($i = 0; $i < $totalPages; $i++) { ?>
                <li <?php if ($page == $i) { ?>
                        echo class = "active"
                    <?php } ?>>
                    <a href="/productos/admin?page=<?php echo $i; ?>">
                        <?php echo $i + 1; ?>
                    </a>
                </li>
            <?php } ?>
            <li>
                <a href="/productos/admin?page=<?php echo $totalPages - 1; ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>

<div class="modal fade" id="modal-product-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>El estado del producto ha cambiado.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>