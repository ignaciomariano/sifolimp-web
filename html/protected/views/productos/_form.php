<?php
$marcas = array("Sifolimp" => "Sifolimp", "Aqcua" => "Aqcua");
#print_r($categorias);exit;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 well well-lg campos-form">
            <div class="form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'productos-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>
                <h2>
                    <?php
                    if ($model->isNewRecord) {
                        echo 'Crea un nuevo producto';
                    } else {
                        echo 'Editar producto';
                    }
                    ?>
                </h2>
                <?php #echo $form->errorSummary($model); ?>

                <div class="input-group">
                    <label>Codigo</label>
                    <?php echo $form->textField($model, 'codigo', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'codigo'); ?>
                </div>
                <div class="input-group">
                    <label>Descripcion</label>
                    <?php echo $form->textField($model, 'descripcion', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'descripcion'); ?>
                </div>                    
                <div class="dropdown">
                    <label>Marca</label>
                    <?php
                    echo $form->dropDownList($model, 'marca', $marcas, array(
                        'prompt' => 'Seleccionar'
                    ));
                    ?>
                    <?php echo $form->error($model, 'marca'); ?>
                </div>
                <div class="input-group">
                    <label>Modelo</label>
                    <?php echo $form->textField($model, 'modelo', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'modelo'); ?>
                </div>
                <div class="input-group">
                    <label>Categoria</label>
                    <?php
                    echo $form->dropDownList($model, 'categoria_id', $categorias, array(
                        'prompt' => 'Seleccionar'
                    ));
                    ?>
                    <?php echo $form->error($model, 'categoria_id'); ?>
                </div>
                <div class="input-group">
                    <label>Embalaje</label>
                    <?php echo $form->textField($model, 'embalaje', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'embalaje'); ?>
                </div>                
                <div class="input-group">
                    <label>Imagen</label>
                    <?php #echo $form->textField($model, 'nombre_foto', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control'));  ?>
                    <!--<input type="file" class="form-control" placeholder="" aria-describedby="basic-addon1">-->
                    <?php echo $form->fileField($model, 'nombre_foto'); ?>
                    <?php echo $form->error($model, 'nombre_foto'); ?>
                </div>
                <div class="input-group">
                    <label>Precio</label>
                    <?php echo $form->textField($model, 'precio', array('size' => 60, 'maxlength' => 6, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'precio'); ?>
                </div>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Cargar' : 'Guardar', array('class' => 'btn-login')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<?php
#if(Yii::app()->user->hasFlash("nombre_foto")) {
#    echo CHtml::image(Yii::app()->request->baseUrl."".Yii::app()->user->getFlash("nombre_foto"));
#}
?>