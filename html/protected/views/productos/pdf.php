<script type="text/javascript" src="/js/base64.js"></script>
<script type="text/javascript" src="/js/sprintf.js"></script>


<script type="text/javascript" src="/js/jspdf.js"></script>
<script type="text/javascript" src="/js/standard_fonts_metrics.js"></script>
<script type="text/javascript" src="/js/split_text_to_size.js"></script>
<script type="text/javascript" src="/js/from_html.js"></script>


<script type="text/javascript">

    function PDF1(){
        var doc = new jsPDF('p', 'in', 'letter');
        var source = $('#pdf').html();
        var specialElementHandlers = {
            '#bypassme': function(element, renderer) {
            return true;
            }
        };

        console.log("hola");

       doc.fromHTML(source, 0.5, 0.5,
        {
            'width': 7.5, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        });

        console.log("hola2");

        doc.output('dataurl');
    }

    $(document).ready(function() {
        //PDF1();
    });


</script>
<!--
<div class="container">
    <div class="btn-login goto" onclick="print()">IMPRIMIR</div>
</div>
-->

<div class="container" id="pdf">
<!--
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="http://www.sifolimp.com.ar/" title="Sifolimp.com.ar">
                    <img alt="Brand" src="/images/sifolimp.gif" alt="Sifolimp" />
                </a>
            </div>
        </div>   
    </nav>   
-->
<?php if ($model != null): ?>
    <table class="table table-striped">
        <tr>
            <td>Código</td>
            <td>Marca</td>                
            <td>Categoría</td>
            <td>Descripción</td>
            <td>Modelo</td>                
            <td>Embalaje</td>
            <td>Precio</td>
        </tr>
        <?php foreach ($model as $data): ?>
            <tr>
                <td><?php echo $data->codigo; ?></td>
                <td><?php echo $data->marca; ?></td>                    
                <td><?php echo $data->categoria->categoria; ?></td>
                <td><?php echo $data->descripcion; ?></td>
                <td><?php echo $data->modelo; ?></td>
                <td><?php echo $data->embalaje; ?></td>
                <td>$<?php echo $data->precio; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
</div>