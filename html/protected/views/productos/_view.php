<div class="view">

    <!--
    <b><?php #echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php #echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />
    -->

    <b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->codigo), array('view', 'id' => $data->id)); ?>
    <?php #echo CHtml::encode($data->codigo); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('marca')); ?>:</b>
    <?php echo CHtml::encode($data->marca); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('categoria')); ?>:</b>
    <?php echo CHtml::encode($data->categoria->categoria); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
    <?php echo CHtml::encode($data->descripcion); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('modelo')); ?>:</b>
    <?php echo CHtml::encode($data->modelo); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('embalaje')); ?>:</b>
    <?php echo CHtml::encode($data->embalaje); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('nombre_foto')); ?>:</b><br />
    <img src="/images/<?php echo CHtml::encode($data->nombre_foto); ?>" width="50"/>
    <br />

</div>