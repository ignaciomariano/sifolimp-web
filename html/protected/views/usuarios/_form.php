<div class="container">

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 well well-lg campos-form">
            <div class="form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'usuarios-form',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <h2>Cliente: <?php echo $model->id; ?> </h2>

                <div class="input-group">
                    <label>Email:</label>
                    <?php echo $form->textField($model, 'email', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
                <div class="input-group">
                    <label>Nombre:</label>
                    <?php echo $form->textField($model, 'nombre', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'nombre'); ?>
                </div>
                <div class="input-group">
                    <label>Apellido:</label>
                    <?php echo $form->textField($model, 'apellido', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'apellido'); ?>
                </div>
                <div
                <?php if (Yii::app()->user->checkAccess('admin')) { ?>
                    class="dropdown"
                <?php } else { ?>
                    class="hidden"
                <?php } ?> >
                    <label>Tipo de usuario:</label>
                    <?php
                    echo $form->dropDownList($model, 'type', $authitems, array(
                        'prompt' => 'Seleccionar'
                    ));
                    ?>
                    <?php echo $form->error($model, 'type'); ?>
                </div>
                <div class="input-group">
                    <label>Razón Social:</label>
                    <?php echo $form->textField($model, 'razon_social', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control', 'readonly' => true)); ?>
                    <?php echo $form->error($model, 'razon_social'); ?>
                </div>
                <div class="input-group">
                    <label>Cuit:</label>
                    <?php echo $form->textField($model, 'cuit', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control', 'readonly' => true)); ?>
                    <?php echo $form->error($model, 'cuit'); ?>
                </div>

                <?php #echo $form->errorSummary($model); ?>
            </div>
            <?php echo CHtml::submitButton('Guardar', array('class' => 'btn-login')); ?>
            <?php $this->endWidget(); ?>

            <?php if($model->id == Yii::app()->user->id){ ?>
                <div class="forgetpass"><a href="/usuarios/changepassword" title="Cambiar contraseña">Cambiar contraseña</a></div>
            <?php } ?>

        </div>
    </div>
</div>