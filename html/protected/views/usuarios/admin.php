<script>

    $(document).ready(function() {
        $('#select-options-tipo').on('change', function() {
            window.location = "/usuarios/admin/tipo/" + this.value;
        });
    });

    function status(data) {
        var user_id = $(data).attr('user_id');
        var status = data.value;
        $.ajax({
            url: '/usuarios/status',
            type: 'POST',
            data: {user_id: user_id, status: status},
            success: function(data) {
                console.log(data);
                $("#modal-user-status").modal();
            },
            error: function(err) {
                console.log(data);
            }
        });
    }

</script>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#usuarios-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid search">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-6 col-sm-3">
                <select class="select-options" id="select-options-tipo">
                    <option selected disabled>Tipo</option>
                    <?php #foreach ($categorias as $categoria) { ?>
                    <option value="4">Todos</option>
                    <option value="0">admin</option>
                    <option value="1">clientes</option>
                    <option value="2">vendedores</option>
                    <?php #if (isset($cate) and $cate != 0) { ?>
                    <?php #if ($categoria->id == $cate) { ?>
                    <!--selected=""-->
                    <?php #} ?>
                    <?php #} ?>
                    <?php #} ?>
                </select>
            </div>
            <a href="/usuarios/create">
                <div class="btn-login goto">Nuevo Cliente
                </div>
            </a>
        </div> <!-- /busqueda -->
    </div><!-- /container-->
</div><!-- /containet fluid-->

<div class="container">
    <?php if ($model != null): ?>
        <table class="table table-striped">
            <tr>
                <td>ID</td>
                <td>Razon Social</td>
                <td>Email</td>            
                <td>Nombre</td>
                <td>Cuit</td>
                <td>Tipo</td>
                <td>Estado</td>
                <td>Editar</td>
            </tr>
            <?php foreach ($model as $data): ?>
                <tr>
                    <td><?php echo $data->id; ?></td>
                    <td><?php echo $data->razon_social; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->apellido . " " . $data->nombre; ?></td>
                    <td><?php echo $data->cuit; ?></td>
                    <td><?php
                        if ($data->type == 0) {
                            echo "admin";
                        } elseif ($data->type == 1) {
                            echo "clientes";
                        } elseif ($data->type == 2) {
                            echo "vendedores";
                        }
                        ?></td>
                    <td>
                        <select class="select-options" onchange="status(this)"
                                user_id="<?php echo $data->id; ?>">
                            <option value="0" 
                            <?php if ($data->status == 0) { ?>
                                        selected <?php } ?>>
                                Deshabilitado
                            </option>
                            <option value="1"
                            <?php if ($data->status == 1) { ?>
                                        selected <?php } ?>>
                                Habilitado</option>
                            <?php #} ?>
                        </select>
                    </td>
                    <td><a href="/usuarios/update/<?php echo $data->id; ?>"
                           class="editbtn" title="editar">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>     
    <?php endif; ?>
</div>


<div class="modal fade" id="modal-user-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>El estado del usuario ha cambiado.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>