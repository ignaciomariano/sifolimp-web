<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 well well-lg campos-form">
            <div class="form">
                <?php
                $form = $this->beginWidget('CActiveForm', array('id' => 'usuarios-form','enableAjaxValidation' => false,));
                ?>
                <div class="input-group ">
                    <label>Antigua password:</label>
                    <?php echo $form->passwordField($model, 'old_password', array('size' => 45, 'maxlength' => 15, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'old_password'); ?>
                </div>
                <div class="input-group ">
                    <label>Nueva password:</label>
                    <?php echo $form->passwordField($model, 'new_password', array('size' => 45, 'maxlength' => 15, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'new_password'); ?>
                </div>
                <div class="input-group ">
                    <label>Repita la nueva password:</label>
                    <?php echo $form->passwordField($model, 'repeat_password', array('size' => 45, 'maxlength' => 15, 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'repeat_password'); ?>
                </div>
            </div>
             <?php echo CHtml::submitButton('Guardar', array('class' => 'btn-login')); ?>
            <div class="opciones-btn">
                <?php if (Yii::app()->user->checkAccess('admin')){ ?> 
                    <a class="btn btn-default" style="width:100%;" href="/pedidos/admin" role="button">Volver</a>
                <?php }else{ ?>
                    <a class="btn btn-default" style="width:100%;" href="/pedidos/mispedidos" role="button">Volver</a>
                <?php } ?>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>    
</div>