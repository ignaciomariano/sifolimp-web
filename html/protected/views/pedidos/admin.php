<style type="text/css">
    .input_container ul {
        width: 206px;
        border: 1px solid #eaeaea;
        position: absolute;
        z-index: 9;
        background: #f3f3f3;
        list-style: none;
    }
    .input_container ul li {
        padding: 2px;
    }
    .input_container ul li:hover {
        background: #eaeaea;
    }
    #country_list_id {
        display: none;
    }
</style>

<script type="text/javascript">

    function changestatus(pedido) {

        var pedido_id = $(pedido).attr('pedido_id');
        var status = pedido.value;
        var msj = "Estas seguro que desea cambiar el status del pedido?";
        if (confirm(msj)) {
            $.ajax({
                url: '/pedidos/status',
                type: 'POST',
                data: {pedido_id: pedido_id, status: status},
                success: function(data) {
                    console.log("OK" + data);
                    if(status == 1){
                        $(pedido).parent().parent().css("color", "#000000");
                    }else{
                        $(pedido).parent().parent().css("color", "green");
                    }
                },
                error: function(err) {
                    console.log("ERR" + err);
                }
            });
        }

    }

    function autocomplet() {
        var min_length = 3; // min caracters to display the autocomplete
        var keyword = $('#client_id').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: '/usuarios/buscar',
                type: 'POST',
                data: {cliente: keyword},
                success: function(data) {
                    //alert(data);
                    $('#client_list_id').show();
                    $('#client_list_id').html(data);
                },
                error: function(err) {
                    $('#client_list_id').show();
                    $('#client_list_id').html(err);
                }
            });
        } else {
            $('#client_list_id').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(id, razon_social) {
        // change input value
        $('#client_id').val(razon_social);
        $("#client_id").attr("client_id", id);
        // hide proposition list
        $('#client_list_id').hide();
    }

    function buscarporclinte() {
        var cliente = $("#client_id").attr("client_id");
        if (!cliente) {
            $("#popup-selecciona-cliente").modal();
        } else {
            window.location = "/pedidos/admin/cliente/" + cliente;
        }
    }

</script>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pedidos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid search">
    <div class="container">
        <div class="row">       
            <div class="col-md-offset-5 col-md-2"></div>
            <?php if (!Yii::app()->user->checkAccess('clientes')) { ?>
                <div class="col-md-3">
                    <div class="input_container">
                        <input type="text" id="client_id" client_id=""
                               class="form-control" onkeyup="autocomplet()"
                               placeholder="Razon social del cliente">
                        <ul id="client_list_id"></ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-login" onclick="buscarporclinte()">Buscar por cliente</div>
                </div>
                <br/><br/><br/>
            <?php } ?>
            <a href="/pedidos/create" >
                <div class="btn-login goto">crear nuevo pedido</div>
            </a>
        </div>
    </div>
</div>



<div class="container">
    <?php if ($model != null): ?>
        <table class="table table-striped">
            <tr>
                <td>Pedido</td>
                <td>Cliente Razon Social</td>
                <td>Fecha</td>
                <td>Status</td>
                <td>Ver</td>
            </tr>
            <?php foreach ($model as $data): ?>
                <tr <?php if ($data->status == 3) { ?> style="color:green;" <?php } ?>>
                    <td><?php echo $data->id; ?></td>
                    <td><?php echo $data->usuarios->razon_social; ?></td>
                    <td><?php echo $data->date; ?></td>
                    <td>
                        <?php if (Yii::app()->user->checkAccess('admin')) { ?>
                            <select class="select-options" pedido_id="<?php echo $data->id; ?>" onchange="changestatus(this)">
                                <option value="1" <?php if ($data->status == 1) { ?>selected <?php } ?>>En proceso</option>
                                <option value="3" <?php if ($data->status == 3) { ?>selected <?php } ?>>Entregado</option>
                                <?php #} ?>
                            </select>
                        <?php } else { ?>
                            <?php
                            if ($data->status == 1) {
                                echo "En proceso";
                            }
                            if ($data->status == 2) {
                                echo "En despacho";
                            }
                            if ($data->status == 3) {
                                echo "Entregado";
                            }
                        }
                        ?>
                    </td>


                    <td>
                        <a href = "/pedidos/view/<?php echo $data->id; ?>" class = "editbtn">
                            <i class = "fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    <nav>
        <ul class="pagination">
            <li>
                <a href="/pedidos/admin<?php
                if (($cli != 0)) {
                    echo "/cliente/" . $cli;
                }
        ?>?page=<?php echo $page - 1; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
                <?php
                for ($i = 0; $i < $totalPages; $i++) {
                    ?>
                <li <?php if ($page == $i) { ?>
                        echo class = "active"
                    <?php } ?>>
                    <a href="/pedidos/admin<?php
                           if (($cli != 0)) {
                               echo "/cliente/" . $cli;
                           }
                           ?>?page=<?php echo $i; ?>">
    <?php echo $i + 1; ?>
                    </a>
                </li>
                <?php } ?>
            <li>
                <a href="/pedidos/admin<?php
                if (($cli != 0)) {
                    echo "/cliente/" . $cli;
                }
                ?>?page=<?php echo $page + 1; ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    <div style="clear: both;"></div>
    *Nota: para pedidos fuera de Capital y GBA el estado "ENTREGADO" implica entregado en transporte seleccionado por el cliente.
</div>

<div class="modal fade" id="popup-selecciona-cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Selecciona un cliente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>