<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pedidos-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <!--
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    -->

    <?php echo $form->errorSummary($model); ?>
    <!--
        <div class="row">
    <?php #echo $form->labelEx($model, 'user_id'); ?>
    <?php #echo $form->textField($model, 'user_id', array('size' => 11, 'maxlength' => 11)); ?>
    <?php #echo $form->error($model, 'user_id'); ?>
        </div>
    
        <div class="row">
    <?php #echo $form->labelEx($model, 'date'); ?>
    <?php #echo $form->textField($model, 'date', array('size' => 45, 'maxlength' => 45)); ?>
    <?php #echo $form->error($model, 'date'); ?>
        </div>
    -->
    <?php
    /*
      $this->widget('zii.widgets.grid.CGridView', array(
      'id' => 'productos-grid',
      'dataProvider' => $modelp->search(),
      'selectableRows' => 2,
      'filter' => $modelp,
      'columns' => array(
      'id',
      'codigo',
      'descripcion',
      'marca',
      'modelo',
      'categoria',
      'embalaje',
      #'nombre_foto',
      array(
      'header' => 'imagen',
      'type' => 'raw',
      'value' => 'CHtml::image("/images/". $data->nombre_foto .".jpg","",array("style"=>"width:25px;height:25px;"))',
      #'value' => '<img src="/images/$data->nombre_foto.jpg" width="50"/>'
      ),
      array(
      'id' => 'selectedIds',
      'class' => 'CCheckBoxColumn'
      ),
      #array(
      #    'class' => 'CButtonColumn',
      #),
      ),
      ));
     */
    ?>

    <div class="row buttons">
        <?php #echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
    <?php #echo CHtml::submitButton('Crear pedido', array('name' => 'ApproveButton')); ?>
    <?php $this->endWidget(); ?>

</div><!-- form -->


<div class="container">

    <div class="btn-login goto" data-toggle="modal" data-target="#datos">Crear Pedido</div>
    <div class="col-sm-12 aviso">
        <p class="text-center">Si no conoces los datos exactos del producto puedes agregar los productos desde la <a href="home-vendedor.html" title="ir a la home"/>Home</a>.<br/>Una vez que hayas seleccionado todos los productos haz clic en "crear pedido"</p>
    </div>
    <?php if ($model != null): ?>
        <table class="table table-striped">
            <tr>
                <td>Id</td>
                <td>Código</td>
                <td>Descripción</td>
                <td>Marca</td>
                <td>Modelo</td>
                <td>categorías</td>
                <td>Embalaje</td>
                <td>Imagen</td>
                <td>Precio</td>
                <td>Agregar</td>
            </tr>            
            <tr>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td><input type="text" class="form-control" ></td>
                <td></td>
            </tr>
            <?php foreach ($model as $data): ?>
                <tr>
                    <td><?php echo $data->id; ?></td>
                    <td><?php echo $data->codigo; ?></td>
                    <td><?php echo $data->descripcion; ?></td>
                    <td><?php echo $data->marca; ?></td>                    
                    <td><?php echo $data->modelo; ?></td>                   
                    <td><?php echo $data->categoria; ?></td>
                    <td><?php echo $data->embalaje; ?></td>
                    <td class="list-image">
                        <img src="/images/<?php echo $data->nombre_foto; ?>" 
                             alt="producto" />                                                    
                    </td>
                    <td><a href="/productos/<?php echo $data->id; ?>"
                           class="editbtn">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    <td><div class="btn btn-default">Agregar</div></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    <!--
    <nav>
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    -->
</div><!-- /container-->