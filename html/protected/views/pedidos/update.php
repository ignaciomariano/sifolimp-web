<?php
$this->breadcrumbs=array(
	'Pedidoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List pedidos', 'url'=>array('index')),
	array('label'=>'Create pedidos', 'url'=>array('create')),
	array('label'=>'View pedidos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage pedidos', 'url'=>array('admin')),
);
?>

<h1>Update pedidos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>