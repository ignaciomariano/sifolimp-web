<script type="text/javascript">
    $(document).ready(function() {
        $("#repetir_pedido").click(function() {
            if (confirm('¿Desea realizar un nuevo pedido igual a este?')) {
                window.location = "/pedidos/repetir/id/<?php echo($pedidos->id); ?>";
            } else {
            }
        });

        /*
         $("#repetir").click(function() {
         if (confirm('¿Desea crear un nuevo pedido a partir de este?')) {
         //$().redirect('/pedidos/repetir2/', {'id': '<?php echo($pedidos->id); ?>'});
         //$.redirect('/pedidos/repetir2/', {'id': '<?php echo($pedidos->id); ?>'});
         window.location = "/pedidos/repetir2/id/<?php echo($pedidos->id); ?>";
         }
         });
         */

    });
    
</script>

<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 well well-lg campos-form">
            <?php if (Yii::app()->user->checkAccess('admin')) { ?>
                <a href="/pedidos/admin" class="btn-login min">Ir a mis pedidos</a>            
            <?php } else { ?>
                <a href="/pedidos/mispedidos" class="btn-login min">Ir a mis pedidos</a>
            <?php } ?>
            <h2>Pedido <?php echo($pedidos->id); ?></h2>
            <ul class="list-unstyled">
                <li><span>Cliente email: </span><?php echo $pedidos->usuarios->email; ?></li>
                <li><span>Cliente razon social:</span><?php echo $pedidos->usuarios->razon_social; ?></li>
                <li><span>Fecha:</span><?php echo $pedidos->date; ?></li>
                <li><span>Status:</span><?php echo $pedidos->status; ?></li>
            </ul> 
            <h2>Productos comprados</h2>
            <div class="well well-sm grey clearfix">
                <table class="table table-condensed">
                    <tr>
                        <td>Id</td>
                        <td>Código</td>
                        <td>Marca</td>
                        <td>Categoría</td>
                        <td>Descripción</td>
                        <td>Modelo</td>
                        <td>Embalaje</td>
                        <td>Imagen</td>
                        <td>Precio</td>
                        <td>Cantidad</td>
                    </tr>
                    <tr>
                        <?php $total = 0; ?>
                        <?php foreach ($pedidosproductos as $pedidoproducto) { ?>
                            <td><?php echo $pedidoproducto->productos->id; ?></td>
                            <td><?php echo $pedidoproducto->productos->codigo; ?></td>
                            <td><?php echo $pedidoproducto->productos->marca; ?></td>
                            <td><?php echo $pedidoproducto->productos->categoria->categoria; ?></td>
                            <td><?php echo $pedidoproducto->productos->descripcion; ?></td>                            
                            <td><?php echo $pedidoproducto->productos->modelo; ?></td>                            
                            <td><?php echo $pedidoproducto->productos->embalaje; ?></td>
                            <td class="list-image">
                                <img src="/images/<?php echo $pedidoproducto->productos->nombre_foto; ?>" alt="producto" />                                                    
                            </td>
                            <td>$<?php echo $pedidoproducto->productos->precio; ?></td>
                            <td><?php echo $pedidoproducto->cant; ?></td>
                            <?php $total += ($pedidoproducto->productos->precio * $pedidoproducto->cant); ?>
                        </tr>

                    <?php } ?>
                </table> 
                <div class="total">
                    <h2>Total</h2>
                    <p>$<?php echo $total; ?></p>
                </div>
            </div>
            <?php if($pedidos->user_id == Yii::app()->user->id){ ?>
            <a href="#" data-toggle="modal" data-target="#popup">
                <div id="repetir" class="btn-login min">Repetir pedido</div>
            </a>
            <?php } ?>
        </div>
    </div>
</div><!-- /container-->

<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Desea crear un nuevo pedido a partir de este?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary"
                        onclick="window.location = '/pedidos/repetir2/id/<?php echo($pedidos->id); ?>'">
                    Aceptar
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>