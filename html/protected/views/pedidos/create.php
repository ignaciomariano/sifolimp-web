<style type="text/css">
    .input_container ul {
        width: 206px;
        border: 1px solid #eaeaea;
        position: absolute;
        z-index: 9;
        background: #f3f3f3;
        list-style: none;
    }
    .input_container ul li {
        padding: 2px;
    }
    .input_container ul li:hover {
        background: #eaeaea;
    }
    #country_list_id {
        display: none;
    }

    .sticky {position: fixed; top: 0; width: 100%; z-index: 100; border-bottom: 4px solid #ffffff; }
</style>
<script type="text/javascript">

    var productos = Array();

    function autocomplet() {
        var min_length = 3; // min caracters to display the autocomplete
        var keyword = $('#client_id').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: '/usuarios/buscar',
                type: 'POST',
                data: {cliente: keyword},
                success: function(data) {
                    //alert(data);
                    $('#client_list_id').show();
                    $('#client_list_id').html(data);
                },
                error: function(err) {
                    $('#client_list_id').show();
                    $('#client_list_id').html(err);
                }
            });
        } else {
            $('#client_list_id').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(id, razon_social) {
        // change input value
        $('#client_id').val(razon_social);
        $("#client_id").attr("client_id", id);
        // hide proposition list
        $('#client_list_id').hide();
    }

    function agregar(producto) {
        var id = $(producto).attr("producto_id");
        var tr = $("#producto_" + id);
        var cant = tr.find("input[name=cant]").val();
        var todo = {};
        if (cant > 0) {
            var result = $.grep(productos, function(e) {
                return e.producto_id == id;
            });
            if (result.length == 0) {
                todo.producto_id = $(producto).attr("producto_id");
                todo.cant = cant;
                console.log(todo);
                productos.push(todo);
                console.log(JSON.stringify(productos));
                $(producto).css("background-color", "#b51a12");
                $(producto).css("color", "white");
                $(producto).text("Quitar");
            } else if (result.length == 1) {
                var index = productos.indexOf(id);
                productos.splice(index, 1);
                console.log(JSON.stringify(productos));
                $(producto).css("background-color", "#fff");
                $(producto).css("color", "#878787");
                $(producto).text("Agregar");
                // access the foo property using result[0].foo
            } else {
                // multiple items found
            }
        } else {
            $("#popup-cantidad").modal();
        }

    }

    function check() {
        var client_id = $("#client_id").attr("client_id");
        if (!client_id) {
            $("#popup").modal();
        } else {
            $.ajax({
                url: '/productos/trae/',
                type: "POST",
                //dataType: 'JSON',
                data: {productos: productos},
                success: function(result)
                {
                    result = JSON.parse(result);
                    var tbody = $('#table-productos');
                    tbody.empty();
                    txt = "<tr>\n\
                            <td>id</td><td>Codigo</td><td>Marca</td>\n\
                            <td>Descripcion</td><td>Modelo</td>\n\
                            <td>embalaje</td>\n\
                            <td>Imagen</td>\n\
                            <td>Cantidad</td>\n\
                            <td>Precio</td></tr>";
                    tbody.append(txt);
                    var txt = "";
                    var tot = 0;
                    $.each(result, function(key, value) {
                        txt = "<tr><td>" + value.id + "</td>\n\
                                <td>" + value.codigo + "</td>\n\
                                <td>" + value.marca + "</td>\n\
                                <td>" + value.descripcion + "</td>\n\
                                <td>" + value.modelo + "</td>\n\
                                <td>" + value.embalaje + "</td>\n\
                                <td><img src='/images/" + value.nombre_foto + "'\n\
                                 height='49' width='49'/></td>\n\
                                <td>" + productos[key].cant + "</td>\n\
                                 <td>" + value.precio + "</td></tr>";
                        tot = parseInt(tot) + (parseInt(value.precio) * productos[key].cant);
                        tbody.append(txt);
                    });
                    $("#total-modal").html(tot);
                }
            });
            $("#popup-confirmacion").modal();
        }
    }

    function crear() {
        var client_id = $("#client_id").attr("client_id");
        $.ajax({
            url: '/pedidos/save/',
            type: "POST",
            data: {productos: productos, client_id: client_id},
            success: function(result)
            {
                console.log(result);
                window.location = "/pedidos/view/" + result;
            }
        });
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('.search').addClass("sticky");
        } else {
            $('.search').removeClass("sticky");
        }
    });

    function buscar() {
        var buscar = $("#buscar_home").val();
        var categoria = $(".select-options").val();
        $.ajax({
            url: '/pedidos/trae/',
            type: "GET",
            data: {q: buscar, c: categoria},
            success: function(result)
            {
                $("#data").html(result);
                $("div[producto_id]").each(function() {
                    var index = productos.indexOf($(this).attr("producto_id"));
                    if (index > -1) {
                        $(this).css("background-color", "#b51a12");
                        $(this).css("color", "white");
                        $(this).text("Quitar");
                    } else {
                        $(this).css("background-color", "#fff");
                        $(this).css("color", "#878787");
                        $(this).text("Agregar");
                    }
                });
                $("#buscar_home").val("");
            }
        });
    }

    $(document).ready(function() {
        buscar();
    });

</script>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pedidos-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="row buttons">
        <?php #echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
    <?php #echo CHtml::submitButton('Crear pedido', array('name' => 'ApproveButton'));  ?>
    <?php $this->endWidget(); ?>

</div><!-- form -->
<div class="container-fluid search">
    <div class="container">
        <div class="row">             
            <div class="col-md-3">                
                <div class="input_container">
                    <?php if (!Yii::app()->user->checkAccess('clientes')) { ?>
                        <input type="text" id="client_id" client_id=""
                               class="form-control" onkeyup="autocomplet()"
                               placeholder="Razon social del cliente">
                        <ul id="client_list_id"></ul>
                    <?php } else { ?>
                        <input type="text" id="client_id" class="form-control"
                               client_id="<?php echo Yii::app()->user->id; ?>"
                               placeholder="<?php echo $razon_social; ?>" readonly/>
                           <?php } ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="btn-login" onclick="check();">Crear Pedido</div>
            </div>
        </div>

        <div style="clear: both;"></div><br/>
        <div class="row">
            <div class="col-sm-3">
                <select class="select-options">
                    <option selected disabled>Categorias</option>
                    <option value="">Todos</option>
                    <?php foreach ($categorias as $categoria) { ?>
                        <option value="<?php echo $categoria->id; ?>"
                        <?php if (isset($cate) and $cate != 0) { ?>
                            <?php if ($categoria->id == $cate) { ?>
                                        selected=""
                                    <?php } ?>
                                <?php } ?>
                                >
                                    <?php echo $categoria->categoria; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" id="buscar_home" class="form-control" 
                    <?php if (isset($desc) and $desc != 0) { ?>
                               placeholder="<?php echo $desc; ?>"
                           <?php } else { ?>
                               placeholder="Buscas algo en particular?"
                           <?php } ?>
                           >
                    <span class="input-group-addon" onclick="buscar()">
                        <i class="fa fa-search"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="btn-login" onclick="buscar()">buscar</div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="container" id="data">
</div><!-- /container-->

<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Selecciona un cliente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="popup-confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>Deseas confirmar este pedido?</h4>
            </div>
            <div class="modal-body">
                <div class="well well-sm grey clearfix">
                    <table class="table table-condensed" id="table-productos">  
                    </table>
                    <div class="total">
                        <h2>total</h2>
                        <p id="total-modal"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="crear();">Confirmar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="popup-cantidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>La cantidad debe ser mayor a 0.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>