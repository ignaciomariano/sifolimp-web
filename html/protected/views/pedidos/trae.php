<?php if ($model != null): ?>
    <table class="table table-striped" id="tabla">
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Marca</th>
                <th>categoría</th>
                <th>Descripción</th>
                <th>Modelo</th>
                <th>Embalaje</th>
                <th>Imagen</th>
                <th>Cantidad</th>
                <th>Agregar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model as $data): ?>
                <tr id="producto_<?php echo $data->id; ?>">
                    <td><?php echo $data->id; ?></td>
                    <td><?php echo $data->codigo; ?></td>
                    <td><?php echo $data->marca; ?></td>
                    <td><?php echo $data->categoria->categoria; ?></td>
                    <td><?php echo $data->descripcion; ?></td>
                    <td><?php echo $data->modelo; ?></td>
                    <td><?php echo $data->embalaje; ?></td>
                    <td class="list-image">
                        <img src="/images/<?php echo $data->nombre_foto; ?>" 
                             alt="producto" />
                    </td>
                    <td><input producto_id="<?php echo $data->id; ?>" 
                               name="cant" value="" size="3" maxlength="3"/></td>
                    <td>
                        <div class="btn btn-default" onclick="agregar(this)" producto_id="<?php echo $data->id; ?>">Agregar</div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>