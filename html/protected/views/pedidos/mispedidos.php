<style type="text/css">
    .input_container ul {
        width: 206px;
        border: 1px solid #eaeaea;
        position: absolute;
        z-index: 9;
        background: #f3f3f3;
        list-style: none;
    }
    .input_container ul li {
        padding: 2px;
    }
    .input_container ul li:hover {
        background: #eaeaea;
    }
    #country_list_id {
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('#select-options-status').on('change', function() {
            var pedido_id = $(this).attr('pedido_id');
            var status = this.value;
            var msj = "Estas seguro que desea cambiar el status del pedido?";
            if (confirm(msj)) {
                $.ajax({
                    url: '/pedidos/status',
                    type: 'POST',
                    data: {pedido_id: pedido_id, status: status},
                    success: function(data) {
                        console.log("OK" + data);
                    },
                    error: function(err) {
                        console.log("ERR" + err);
                    }
                });
            }

        });

    });

    function autocomplet() {
        var min_length = 3; // min caracters to display the autocomplete
        var keyword = $('#client_id').val();
        if (keyword.length >= min_length) {
            $.ajax({
                url: '/usuarios/buscar',
                type: 'POST',
                data: {cliente: keyword},
                success: function(data) {
                    //alert(data);
                    $('#client_list_id').show();
                    $('#client_list_id').html(data);
                },
                error: function(err) {
                    $('#client_list_id').show();
                    $('#client_list_id').html(err);
                }
            });
        } else {
            $('#client_list_id').hide();
        }
    }

// set_item : this function will be executed when we select an item
    function set_item(id, razon_social) {
        // change input value
        $('#client_id').val(razon_social);
        $("#client_id").attr("client_id", id);
        // hide proposition list
        $('#client_list_id').hide();
    }

    function buscarporclinte() {
        var cliente = $("#client_id").attr("client_id");
        if (!cliente) {
            alert("Selecciona un cliente");
        } else {
            window.location = "/pedidos/admin/cliente/" + cliente;
        }
    }

</script>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pedidos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid search">
    <div class="container">
        <div class="row">       
            <div class="col-md-offset-5 col-md-2"></div>
            <?php if (!Yii::app()->user->checkAccess('clientes')) { ?>
                <div class="col-md-3">
                    <div class="input_container">
                        <input type="text" id="client_id" client_id=""
                               class="form-control" onkeyup="autocomplet()"
                               placeholder="Razon social del cliente">
                        <ul id="client_list_id"></ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-login" onclick="buscarporclinte()">Buscar por cliente</div>
                </div>
                <br/><br/><br/>
            <?php } ?>
            <a href="/pedidos/create" >
                <div class="btn-login goto">crear nuevo pedido</div>
            </a>
        </div>
    </div>
</div>



<div class="container">
    <?php if ($model != null): ?>
        <table class="table table-striped">
            <tr>
                <td>Pedido</td>
                <td>Cliente Razon Social</td>
                <td>Fecha</td>                
                <td>Status</td>
                <td>Ver</td>
            </tr>
            <?php foreach ($model as $data): ?>
                <?php #print_r($data); ?>
                <?php #print_r($data->usuarios); ?>
                <?php #print_r($data->pedidos); ?>
                <?php #exit; ?>
                <tr>
                    <td><?php echo $data->id; ?></td>
                    <td><?php echo $data->usuarios->razon_social; ?></td>
                    <td><?php echo $data->date; ?></td>
                    <td>
                        <?php if ($data->status == 1) { ?>En proceso<?php } ?>
                        <?php if ($data->status == 2) { ?>En despacho <?php } ?>
                        <?php if ($data->status == 3) { ?>Entregado<?php } ?>
                    </td>
                    <td>
                        <a href = "/pedidos/view/<?php echo $data->id; ?>" class = "editbtn">
                            <i class = "fa fa-eye"></i>
                        </a>
                    </td>
                    <!--
                    <td>
                    <a href = "#" class = "editbtn">
                    <i class = "fa fa-trash"></i>
                    </a>
                    </td>
                    -->
                </tr>
                <?php #echo $data->name;  
                ?>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>


    <?php #echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));        ?>
    <div class="search-form" style="display:none">
        <?php
        /*
          $this->renderPartial('_search', array(
          'model' => $model,
          ));
         */
        ?>
    </div><!-- search-form -->    
    <nav>
        <ul class="pagination">
            <li>
                <a href="/pedidos/mispedidos?page=<?php echo 0; ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <?php for ($i = 0; $i < $totalPages; $i++) { ?>
                <li <?php if ($page == $i) { ?>
                        echo class = "active"
                    <?php } ?>>
                    <a href="/pedidos/mispedidos?page=<?php echo $i; ?>">
                           <?php echo $i + 1; ?>
                    </a>
                </li>
            <?php } ?>
            <li>
                <a href="/pedidos/mispedidos?page=<?php echo $totalPages-1; ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    <div style="clear: both;"></div>
    *Nota: para pedidos fuera de Capital y GBA esto implica entregado en transporte seleccionado por el cliente.
</div>
<?php
/*
  $this->widget('zii.widgets.grid.CGridView', array(
  'id' => 'pedidos-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
  #'id',
  #'user_id',
  array(
  'header' => 'Razon social',
  'name' => 'razon_social',
  'value' => '$data->usuarios->razon_social',
  'filter' => CHtml::activeTextField($model, 'razon_social'),
  ),
  array(
  'header' => 'nombre',
  'name' => 'nombre',
  'value' => '$data->usuarios->nombre',
  ),
  array(
  'header' => 'apellido',
  'name' => 'apellido',
  'value' => '$data->usuarios->apellido',
  ),
  'date',
  #array(
  #    'class' => 'CButtonColumn',
  #),
  array(
  'header' => '',
  'class' => 'CButtonColumn',
  'viewButtonImageUrl' => Yii::app()->baseUrl . '/css/gridViewStyle/images/' . 'gr-view.png',
  #'updateButtonImageUrl' => Yii::app()->baseUrl .
  #'/css/gridViewStyle/images/' . 'gr-update.png',
  'updateButtonImageUrl' => "<i class='fa fa-pencil'></i>",
  #<a href="editar-pedido.html" class="editbtn">
  #<i class="fa fa-pencil"></i></a>
  'deleteButtonImageUrl' => Yii::app()->baseUrl . '/css/gridViewStyle/images/' . 'gr-delete.png',
  ),
  ),
  ));
 */
?>