<?php
$this->breadcrumbs = array(
    'Pedidoses',
);

$this->menu = array(
    array('label' => 'Create pedidos', 'url' => array('create')),
    array('label' => 'Manage pedidos', 'url' => array('admin')),
);
?>

<h1>Pedidoses</h1>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
    #'pager' => array("htmlOptions"=>array("class"=>"pagination")),
));
?>
