<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('Pedido')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <!--
    <b><?php #echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
    <?php #echo CHtml::encode($data->user_id); ?>
    <br />
    -->
    
    <b><?php echo CHtml::encode($data->getAttributeLabel('usuario')); ?>:</b>
    <?php echo CHtml::encode($data->usuarios->razon_social) . ", " . 
            CHtml::encode($data->usuarios->cuit); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
    <?php echo CHtml::encode($data->date); ?>
    <br />

    <!--
    <?php #foreach ($data->productos as $pp): ?>
        <h3>pedido_id :<?php #echo ($pp->pedido_id); ?></h3>
        <h3>producto_id :<?php #echo ($pp->producto_id); ?></h3>
    <?php #endforeach; ?>    
    <br/>
    -->

</div>