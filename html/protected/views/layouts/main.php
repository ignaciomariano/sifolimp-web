<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="en">

        <!-- blueprint CSS framework -->
        <!--
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl;  ?>/css/screen.css" media="screen, projection">
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl;  ?>/css/print.css" media="print">
        -->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
        <![endif]-->

        <!--
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl;  ?>/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl;  ?>/css/form.css">
        -->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" >
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="http://www.sifolimp.com.ar/" title="Sifolimp.com.ar">
                        <img alt="Brand" src="img/sifolimp.gif" alt="Sifolimp" />
                    </a>
                </div>


                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="home-vendedor.html">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pedidos <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="pedidos-vendedor.html">lista de pedidos<i class="fa fa-bars"></i></a></li>
                            <li><a href="crear-pedidos-vendedor.html">crear pedidos<i class="fa fa-plus"></i></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Productos <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="productos-vendedor.html">lista de productos<i class="fa fa-bars"></i></a></li>
                            <li><a href="crear-producto-vendedor.html">crear productos<i class="fa fa-plus"></i></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Clientes <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="clientes-vendedor.html">lista de clientes<i class="fa fa-bars"></i></a></li>
                            <li><a href="crear-cliente-vendedor.html">crear clientes<i class="fa fa-plus"></i></a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav><!-- /. nav-->


        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">                
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Homeee', 'url' => array('/site/index'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Pedidos', 'url' => array('/pedidos/index'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Productos', 'url' => array('/productos/index'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Usuariosss', 'url' => array('/usuarios/index'), 'visible' => !Yii::app()->user->isGuest),
                        #array('label' => 'About', 'url' => array('/site/page', 'view' => 'about')),
                        #array('label' => 'Contact', 'url' => array('/site/contact')),
                        array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Logout (' . Yii::app()->user->name . /* Yii::app()->user->rol . */')',
                            'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                    ),
                ));
                ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <div class="footer container-fluid">
                    <div class="container">
                        ©2013 Sifolimp
                    </div>
                </div>
            </div><!-- footer -->

        </div><!-- page -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/wow.min.js"></script>
    </body>
</html>
