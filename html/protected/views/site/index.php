<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;
?>

<style type="text/css">
    #test { 
        height: 30px;
        width: 150px;
        border: 1px #000 solid;
    }
    #test li { padding: 5px 10px; z-index: 2; }
    #test li:not(.init) { float: left; width: 130px; display: none; background: #ddd; }
    #test li:not(.init):hover, #test li.selected:not(.init) { background: #09f; }
    li.init { cursor: pointer; }

    a#submit { z-index: 1; }

    .sticky {position: fixed; top: 0; width: 100%; z-index: 100; border-bottom: 4px solid #ffffff; }
</style>

<script type="text/javascript">

    function buscar() {
        var buscar = $("#buscar_home").val();
        var categoria = $(".select-options").val();
        console.log(categoria);
        if (categoria && buscar) {
            window.location = "/site/index/c/" + categoria + "/q/" + buscar;
        } else if (categoria && !buscar) {
            window.location = "/site/index/c/" + categoria;
        } else if (!categoria && buscar) {
            window.location = "/site/index/q/" + buscar;
        }
    }

    function select(obj) {
        alert(obj);
        //var buscar = $("#buscar_home").val();
        //window.location = "/site/index/q/" + buscar;
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('.search').addClass("sticky");
        }
        else {
            $('.search').removeClass("sticky");
        }
    });</script>

<div class="container-fluid search">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-4 col-sm-3">
                <!--<select class="selectpicker">-->
                <select class="select-options">
                    <option selected disabled>Categorias</option>
                    <?php foreach ($categorias as $categoria) { ?>
                        <option value="<?php echo $categoria->id; ?>"
                        <?php if (isset($cate) and $cate != 0) { ?>
                            <?php if ($categoria->id == $cate) { ?>
                                        selected=""
                                    <?php } ?>
                                <?php } ?>
                                >
                                    <?php echo $categoria->categoria; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" id="buscar_home" class="form-control" 
                    <?php if (isset($desc) and $desc != 0) { ?>
                               placeholder="<?php echo $desc; ?>"
                           <?php } else { ?>
                               placeholder="Buscas algo en particular?"
                           <?php } ?>
                           >
                    <span class="input-group-addon" onclick="buscar()">
                        <i class="fa fa-search"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="btn-login" onclick="buscar()">buscar</div>
            </div>
        </div> <!-- /busqueda -->
    </div><!-- /container-->
</div><!-- /containet fluid-->
<div class="container">
    <div class="articulos clearfix">
        <h2>Productos</h2>
        <?php if ($model != null) { ?>
            <ul class="articulos-list buy" id="lista-pedidos">
                <?php foreach ($model as $data): ?>
                    <?php #print_r($data);exit; ?>
                    <li>
                        <div class="producto-imagen">
                            <img src="/images/<?php echo $data->nombre_foto; ?>" 
                                 alt="producto" />
                        </div>
                        <p><span>Codigo:</span><?php echo $data->codigo; ?> </p>
                        <p><span>Marca: </span><?php echo $data->marca; ?> </p>
                        <p><span>Categoria: </span><?php echo $data->categoria->categoria; ?></p>
                        <p><span>Descripcion:</span><?php echo substr($data->descripcion, 0, 15); ?> </p>                      
                        <p><span>Modelo:</span><?php echo substr($data->modelo, 0, 20); ?><p>

                        <p><span>Embalaje:</span> <?php echo substr($data->embalaje, 0, 16); ?></p>
                    </li>
                <?php endforeach; ?>
                <div style="clear: both; height: 10px;"></div>
            </ul>
        <?php }else { ?>
            <h2>No se encontraron productos con esa descripcion</h2>
            <div class="col-sm-2">
                <div class="btn-login"><a href="/site/index" style="color:white;">Ir al inicio</a></div>
            </div>

        <?php } ?>
    </div>
</div>