<script type="text/javascript">

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }

    //function cerrar() {
    //$('#passwordforgotten').modal('hide');
    //}


    $(document).ready(function() {
        $(".forgetpass").click(function() {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            var email = $(".form-control").val();
            console.log(email);
            var result = pattern.test(email);
            if (result === true) {
                $.ajax({
                    url: '/usuarios/enviar',
                    type: 'POST',
                    data: {email: email},
                    success: function(data) {
                        //console.log("OK " + data);
                        $("#errorlogin").show().html(data).delay(6000).fadeOut();
                        //$("#enviar_password_forgotten").click(false);
                        //$("#enviar_password_forgotten").hide();
                        //setInterval(function(){cerrar()}, 6000);
                    },
                    error: function(err) {
                        console.log("ERROR " + err.responseText);
                    }
                });
            } else {
                $("#errorlogin").show().html("Ingrese un mail valido").delay(4000).fadeOut();
            }


        });
    });

</script>

<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
?>

<div class="logo">
    <img src="/images/sifolimp2.gif" alt="Sifolimp" /> 
</div>
<div class="login-box clearfix">
    <div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>

        <div class="content-form">
            <form class="email-login">
                <label>Email:</label>
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <?php #echo $form->labelEx($model, 'Email'); ?>
                    <!--<input class="form-control" type="text" />-->
                    <div id="username">
                        <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
                    </div>
                    <?php echo $form->error($model, 'username'); ?>
                </div>
                <label>Contraseña:</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <!--<input class="form-control" type="password" />-->
                    <div style="clear: both;"></div>
                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
                <?php #echo $form->checkBox($model, 'rememberMe'); ?>
                <?php #echo $form->label($model, 'rememberMe'); ?>
                <?php #echo $form->error($model, 'rememberMe'); ?>
                <div class="forgetpass"><a href="#" title="olvide mi contraseña">Olvide mi contraseña</a></div>
                &nbsp;<p id="errorlogin" class="min"></p>
                <?php
                if ($model->hasErrors()) {
                    #echo CHtml::errorSummary($model);
                }
                ?>

                <?php echo CHtml::submitButton('INGRESAR', array('class' => 'btn-login')); ?>
                <!--
                <div class="goto-register">No tienes cuenta? 
                    <span><a href="/usuarios/create">Crear cuenta</a></span>
                </div>
                -->
            </form>
            <?php $this->endWidget(); ?>    
        </div><!-- /.content-form-->
    </div><!-- Modal -->
</div>

<!-- Modal -->
<!--
<div class="modal fade" id="passwordforgotten" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Escribe tu email para que te enviemos una nueva contraseña</h4>
            </div>
            <div class="modal-body">
                <div class="content-form">
                    <form class="email-login">
                        <div class="input-group margin-bottom-sm">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                            <input class="form-control" type="text" id="email_password_forgotten"/>
                        </div>
                    </form>
                </div>
            </div>
            &nbsp;<p id="errorlogin" class="min"></p>
            <div class="modal-footer">
                <div class="btn-login" id="enviar_password_forgotten">Enviar</div>
            </div>
        </div>
    </div>
</div>
-->