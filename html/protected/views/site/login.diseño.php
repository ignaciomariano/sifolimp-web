<div class="logo">
    <img src="/images/sifolimp2.gif" alt="Sifolimp" /> 
</div>
<div class="login-box clearfix">
    <div class="lb-header">
        <div class="box-btn">
            <div href="#" class="active" id="login-box-link">Soy cliente</div>
        </div>
        <div class="box-btn">
            <div href="#" id="signup-box-link">Soy vendendor</div>
        </div>
    </div>


    <div class="content-form">
        <form class="email-login">
            <label>Email:</label>
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                <input class="form-control" type="text" />
            </div>
            <label>Contraseña:</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                <input class="form-control" type="password" />
            </div>
            <div class="forgetpass">
                <a href="#" title="olvide mi contraseña">Olvide mi contraseña</a>
            </div>
            <a href="home-cliente.html" title="iniciar sesion como cliente"><div class="btn-login">Ingresar</div></a>
            <div class="goto-register">No tienes cuenta? <span><a href="register.html">Crear cuenta</a></span></div>
        </form><!-- /. login de clientes-->


        <form class="email-signup">
            <label>Email:</label>
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                <input class="form-control" type="text" />
            </div>
            <label>Contraseña:</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                <input class="form-control" type="password" />
            </div>
            <div class="forgetpass">
                <a href="#" title="olvide mi contraseña">Olvide mi contraseña</a>
            </div>
            <a href="home-vendedor.html" title="iniciar sesion como vendedor"><div class="btn-login">Ingresar</div></a>
            <div class="goto-register">No tienes cuenta? <span><a href="register.html">Crear cuenta</a></span></div>
        </form><!-- cierra login vendedor -->
    </div><!-- /.content-form-->
</div>

<script>
    $(".email-signup").hide();
    $("#signup-box-link").click(function() {
        $(".email-login").fadeOut(100);
        $(".email-signup").delay(100).fadeIn(100);
        $("#login-box-link").removeClass("active");
        $("#signup-box-link").addClass("active");
    });
    $("#login-box-link").click(function() {
        $(".email-login").delay(100).fadeIn(100);
        ;
        $(".email-signup").fadeOut(100);
        $("#login-box-link").addClass("active");
        $("#signup-box-link").removeClass("active");
    });
</script>