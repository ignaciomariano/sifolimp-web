<?php

class PedidosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('update', 'index', 'view', 'admin', 'delete', 'save', 'ajax', 'confirmar', 'repetir', 'repetir2', 'productospedido', 'status', 'mispedidos'),
                'roles' => array('admin'),
            ),
            array('allow',
                'actions' => array('update', 'index', 'view', 'save', 'ajax', 'confirmar', 'repetir', 'repetir2', 'productospedido', 'status', 'mispedidos'),
                'roles' => array('clientes'),
            ),
            array('allow',
                'actions'=>array('create','trae'),
                'users'=>array('*'),
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView() {
        $id = Yii::app()->request->getParam('id');
        $pedidos = pedidos::model()->with('usuarios')->findByPk($id);
        $pedidosproductos = pedidosproductos::model()->with('productos')->findAll(array(
            'condition' => 'pedido_id=:pedido_id',
            'params' => array(':pedido_id' => $id)
        ));

        $this->render("view", array(
            "pedidos" => $pedidos,
            "pedidosproductos" => $pedidosproductos
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $usuario = usuarios::model()->find(array(
            'select' => 'razon_social',
            'condition' => 'id=:id',
            'params' => array(':id' => Yii::app()->user->id),
        ));

        $model = productos::model()->with('categoria')->findAll(array('order' => 't.id desc'));
        $categorias = categorias::model()->findAll();

        if (isset($_GET['productos']))
            $model->attributes = $_GET['productos'];

        $this->render('create', array(
            'model' => $model,
            'categorias' => $categorias,
            'razon_social' => $usuario->razon_social,
        ));
    }

    public function actionTrae() {
        $status=1;
        $z = new CDbCriteria();
        $c = is_null($_GET["c"]) ? 0 : $_GET["c"];
        $q = isset($_GET["q"]) ? $_GET["q"] : 0;

        if ($c != 0) {
            $z->join = " INNER JOIN categorias c ON c.id = t.categoria_id ";
            $z->addCondition("t.categoria_id = $c");
            $z->addCondition('t.status = :status');
            $z->params = array(':status' => 1);
        }

        if ($q != "0") {
            $z->join = " INNER JOIN categorias c ON c.id = t.categoria_id ";
            $z->addCondition("descripcion LIKE '%$q%' "
                    . "or modelo LIKE '%$q%' "
                    . "or c.categoria LIKE '%$q%'");
            $z->addCondition('t.status = :status');
            $z->params = array(':status' => 1);
        }

        $model = productos::model()->findAll($z);
        $this->renderPartial('trae', array(
            'model' => $model,
        ));
    }

    public function actionSave() {

        $pedidos = new pedidos();
        $cliente_id = $_POST['client_id'];
        $clientes = usuarios::model()->findByPk($cliente_id);
        $email = $clientes->email;
        $pedidos->user_id = $cliente_id;
        $pedidos->date = date('Y-m-d H:i:s');
        $pedidos->status = 1;
        $pedidos->save();
        $prod = $_POST['productos'];
        foreach ($prod as $p) {
            $productos = new pedidosproductos();
            $productos->pedido_id = $pedidos->id;
            $productos->producto_id = $p["producto_id"];
            $productos->cant = $p["cant"];
            $productos->save();
        }
        $subject = "Su pedido ha sido realizado";
        $txt = "Gracias por utilizar nuestro sistema online de pedidos,"
                . " su pedido est&aacute; siendo procesado"
                . "<br/><br/><br/>SIFOLIMP-pedidos web.";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: info@sifolimp.com.ar\r\nReply-To: info@sifolimp.com.ar";
        $res = mail($email, $subject, $txt, $headers);
        if ($res == 1) {
            #echo "Se ha enviado su nueva password al mail.";
        } else {
            #echo "Ha fallado el envio de su nueva password, contactese con el administrador.";
        }

        echo $pedidos->id;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {
        $model = $this->loadModel();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['pedidos'])) {
            $model->attributes = $_POST['pedidos'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */

    public function actionIndex() {

        $criteria = new CDbCriteria(array(
            'order' => 't.id DESC',
            'with' => array('usuarios')
        ));
        $dataProvider = new CActiveDataProvider('pedidos', array(
            'criteria' => $criteria,
                )
        );

        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {

        #$model = new pedidos('search');
        #$model->unsetAttributes();  // clear any default values
        if (isset($_GET['pedidos']))
            $model->attributes = $_GET['pedidos'];

        $limit = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $offset = $limit * $page;
        $user_id = Yii::app()->user->id;

        if (isset($_GET['cliente'])) {
            
            $cli = $_GET['cliente'];
            $count = pedidos::model()->findAll(
                    array(
                        "condition" => "user_id =  $user_id",
                    )
            );
            $totalItems = count($count);
            $model = pedidos::model()->findAll(
                    array(
                        "condition" => "user_id =  $cli",
                        "order" => "id desc",
                        'limit' => $limit,
                        'offset' => $offset
                    )
            );
        } else {
            $count = pedidos::model()->findAll(
                    array(
                        'order' => 'id desc',
                    )
            );
            $model = pedidos::model()->findAll(
                    array(
                        'order' => 'id desc',
                        'limit' => $limit,
                        'offset' => $offset
                    )
            );
            $totalItems = count($count);
        }
        $cli = (isset($cli)) ? $cli : 0;
        $div = $totalItems / $limit;
        $totalPages = round($div, 0);
        $this->render('admin', array(
            'model' => $model,
            'page' => $page,
            'totalPages' => $totalPages,
            'cli' => $cli
        ));
    }

    public function actionMispedidos() {
        $limit = 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $offset = $limit * $page;
        $user_id = Yii::app()->user->id;
        $count = pedidos::model()->findAll(
                array(
                    "condition" => "user_id =  $user_id",
                )
        );
        $totalItems = count($count);
        $model = pedidos::model()->findAll(
                array(
                    "condition" => "user_id =  $user_id",
                    "order" => "id desc",
                    'limit' => $limit,
                    'offset' => $offset
                )
        );
        $div = $totalItems / $limit;
        $totalPages = round($div, 0);
        #print_r($totalItems . " " . $div . " " . $totalPages);
        $this->render('mispedidos', array(
            'model' => $model,
            'page' => $page,
            'totalPages' => $totalPages,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id']))
                $this->_model = pedidos::model()->findbyPk($_GET['id']);
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pedidos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionConfirmar() {
        $id = $_GET['id'];
        $pedidos = pedidos::model()->findByPk($id);
        $pedidos->status = 2;
        $pedidos->update();
        $this->redirect(array('admin'));
    }

    public function actionRepetir() {
        $pedidos = new pedidos();
        $pedidos->user_id = Yii::app()->user->id;
        $pedidos->date = date('Y-m-d H:i:s');
        $pedidos->status = 1;
        $pedidos->save();
        $id = $_GET['id'];
        $productos_repetir = pedidosproductos::model()->findAll("pedido_id = $id");
        #print_r($productos_repetir);exit;
        foreach ($productos_repetir as $p) {
            $productos = new pedidosproductos();
            $productos->pedido_id = $pedidos->id;
            $productos->producto_id = $p->producto_id;
            $productos->cant = $p->cant;
            #print_r($productos);exit;
            $productos->save();
        }
        $this->redirect(array('admin'));
        #echo $pedidos->id;
    }

    public function actionRepetir2() {
        $id = $_GET['id'];
        $pedido = pedidos::model()->with('usuarios')->find(array(
            #'select' => 't.razon_social',
            'condition' => 't.id=:id',
            'params' => array(':id' => $id),
        ));
        #print_r($pedido);exit;

        if (Yii::app()->user->checkAccess('clientes') and $pedido->user_id != Yii::app()->user->id) {
            $this->redirect(array('mispedidos'));
        }

        $categorias = categorias::model()->findAll();
        if (isset($_GET['productos']))
            $model->attributes = $_GET['productos'];

        $this->render('repetir2', array(
            'id' => $id,
            'categorias' => $categorias,
            'razon_social' => $pedido->usuarios->razon_social,
            'user_id' => $pedido->usuarios->id,
        ));
    }

    public function actionProductospedido() {
        $id = $_GET['id'];
        $pedido_productos = pedidosproductos::model()->findAll("pedido_id = $id");
        $productos = array();
        foreach ($pedido_productos as $pp) {
            $producto = new stdClass();
            $producto->producto_id = $pp->producto_id;
            $producto->cant = $pp->cant;
            array_push($productos, $producto);
        }

        $productos_j = json_encode($productos);
        print_r($productos_j);
        exit;
    }

    public function actionStatus() {
        $pedido_id = $_POST["pedido_id"];
        $status = $_POST["status"];
        $pedidos = pedidos::model()->findbyPk($pedido_id);
        $pedidos->status = $status;
        if ($pedidos->save()) {
            echo "OK";
        } else {
            echo "ERROR";
        }
    }

}
