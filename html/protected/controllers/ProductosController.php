<?php

class ProductosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                #'controllers' => array("users"),
                'actions' => array('create','update', 'index', 'view', 'admin', 'delete', 'UpdatePrecio','pdf', 'pdf2','status'),
                'roles' => array('admin'),
            ),

            array('allow',
                'actions'=>array('trae'),
                'users'=>array('*'),
            ),

            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView() {
        $this->render('view', array(
            'model' => $this->loadModel(),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new productos;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['productos'])) {
            $model->setScenario('create');
            $model->attributes = $_POST['productos'];
            $res = $model->validate();
            $res2 = $model->getErrors();
            $uf = CUploadedFile::getInstance($model, 'nombre_foto');
            $name = $uf ? $uf->getName() : NULL;
            $model->nombre_foto = $name;
            if ($model->save()) {
                if ($uf) {
                    $uf->saveAs(Yii::getPathOfAlias("webroot") . "/images/" . $name);
                }
                $this->redirect(array('admin'));
            }
        }
        $categorias = categorias::model()->findAll();
        $cate = Array();

        foreach ($categorias as $categoria) {
            $cate[$categoria->id] = $categoria->categoria;
        }

        $this->render('create', array(
            'model' => $model,
            'categorias' => $cate,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {
        $model = $this->loadModel();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['productos'])) {
            $model->setScenario('update');
            $model->attributes = $_POST['productos'];
            $uf = CUploadedFile::getInstance($model, 'nombre_foto');
            $model->nombre_foto = $uf ? $uf->getName() : $model->nombre_foto;
            if ($model->save()) {
                if ($uf) {
                    $uf->saveAs(Yii::getPathOfAlias("webroot") . "/images/" . $model->nombre_foto);
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $categorias = categorias::model()->findAll();
        $cate = Array();
        foreach ($categorias as $categoria) {
            $cate[$categoria->id] = $categoria->categoria;
        }
        $this->render('update', array(
            'model' => $model,
            'categorias' => $cate,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $productos = productos::model()->findByPk($id);
            $productos->delete();
            $this->redirect(array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('productos');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */

    public function actionAdmin() {

        if (isset($_GET['productos']))
            $model->attributes = $_GET['productos'];


        $limit = 15;
        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $offset = $limit * $page;
        $model = productos::model()->with('categoria')->findAll(
                array(
                    'order' => 't.id desc',
                    'limit' => $limit,
                    'offset' => $offset
                )
        );
        $totalItems = productos::model()->count();
        $div = $totalItems / $limit;
        $totalPages = round($div, 0);
        $this->render('admin', array(
            'model' => $model,
            'page' => $page,
            'totalPages' => $totalPages,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id']))
                $this->_model = productos::model()->findbyPk($_GET['id']);
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'productos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionUpdatePrecio() {
        if (isset($_POST['id']) and isset($_POST['precio'])) {
            $id = $_POST['id'];
            $precio = $_POST['precio'];
            $productos = productos::model()->findByPk($id);
            $productos->precio = $precio;
            $productos->save();
        }else{
            echo "Faltan datos";
        }      
    }

    public function actionTrae() {
        $p = $_POST['productos'];
        $ids = Array();
        foreach ($p as $pp) {
            array_push($ids, $pp['producto_id']);
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition("id", $ids);
        $result = productos::model()->findAll($criteria);
        #print_r(json_encode($result));exit;
        echo CJSON::encode($result);exit;
    }

    public function actionPdf() {

        $model = productos::model()->with('categoria')->findAll(
                array(
                    'order' => 't.id desc',
                )
        );

        $this->render('pdf', array(
            'model' => $model,
        ));
    }

       public function actionPdf2() {

        $model = productos::model()->with('categoria')->findAll(
                array(
                    'order' => 't.id desc',
                )
        );

        $pdf=Yii::app()->pdfFactory->getTCPDF(); 
        $tbl = "";
        $con=mysqli_connect("localhost","root","root");  
        mysqli_select_db($con, "sifolimp_db");  
        $result = mysqli_query($con,"SELECT codigo,descripcion,marca,modelo,embalaje,precio from productos limit 2");
        if (($result)){  
            $tbl .= "<div>";
            if (mysqli_num_rows($result)>0){   
                $tbl .= "<div>";  
                while ($property =  mysqli_fetch_field($result)){  
                    $tbl .= "<div><b>" . $property->name . "</b></div>"; 
                }  
                $tbl .= "</div>";
                while ($rows = mysqli_fetch_array($result,MYSQL_ASSOC)){
                    $tbl .= "<div>";
                    foreach ($rows as $data){  
                        $tbl .= "<div>" . $data . "</div>";  
                    }
                    $tbl .= "</div>";
                }  
            }else{  
            }
            $tbl .= "</div>";
            $pdf->writeHTML($tbl, true, false, true, false);
        }else{  
            #echo "Error in running query :". mysql_error(); 
        }

        $pdf->Output('example_011.pdf', 'I');

    }


    public function actionStatus() {
        $product_id = $_POST["product_id"];
        $status = $_POST["status"];

        $result = productos::model()->updateByPk($product_id, array(
            'status' => $status
        ));
        if ($result) {
            echo "OK";
        } else {
            echo "ERROR";
        }
    }

}
