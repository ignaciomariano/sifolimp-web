<?php

class UsuariosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('init', 'create', 'update', 'index', 'view', 'admin', 'delete', 'assign', 'buscar', 'enviar', 'status'),
                'roles' => array('admin'),
            ),
            array('allow',
                'actions' => array('update', 'index', 'view', 'delete', 'assign', 'buscar', 'status'),
                'roles' => array('clientes'),
            ),
            array('allow',
                'actions' => array('update', 'index', 'view', 'delete', 'assign', 'buscar', 'status'),
                'roles' => array('vendedores'),
            ),
            array('allow',
                'actions' => array('enviar'),
                'users' => array('*'),
            ),                        
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView() {
        $this->render('view', array(
            'model' => $this->loadModel(),
        ));
    }

    public function actionInit() {
        $url = $this->createUrl('usuarios/update', array('id' => 21));
        echo $url;
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        #Uncomment the following line if AJAX validation is needed
        #$this->performAjaxValidation($model);

        $model = new usuarios;
        $authitems = Array();
        foreach (Yii::app()->authManager->getAuthItems() as $data) {
            $authitems[$data->type] = $data->name;
        }

        if (isset($_POST['usuarios'])) {

            $model->email = $_POST['usuarios']['email'];
            $model->password = $_POST['usuarios']['password'];
            $model->razon_social = $_POST['usuarios']['razon_social'];
            $model->cuit = $_POST['usuarios']['cuit'];
            $model->nombre = $_POST['usuarios']['nombre'];
            $model->apellido = $_POST['usuarios']['apellido'];
            $model->type = $_POST['usuarios']['type'];

            if ($model->save()) {
                $this->actionAssign($model->id, $authitems[$model->type]);
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'authitems' => $authitems,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {


        if (isset($_GET['id'])){
            var_dump("chau");exit;
            $model = $this->loadModel();
        }else{
            $model = usuarios::model()->findByPk(Yii::app()->user->id);
        }


        $authitems = Array();
        foreach (Yii::app()->authManager->getAuthItems() as $data) {
            $authitems[$data->type] = $data->name;
        }

        if (isset($_POST['usuarios'])) {
            $model->setScenario('update');
            $model->attributes = $_POST['usuarios'];
            if ($model->save()) {
                foreach (Yii::app()->authManager->getAuthItems() as $data) {
                    Yii::app()->authManager->revoke($data->name, $model->id);
                }
                if($_POST['usuarios']['type']){
                    $this->actionAssign($model->id, $authitems[$_POST['usuarios']['type']]);
                }
                if (Yii::app()->user->checkAccess('admin')) {
                    $this->redirect(array('admin'));
                } else {
                    $this->redirect("/pedidos/mispedidos/");
                }
            }
        }

        if (Yii::app()->user->checkAccess('clientes') || Yii::app()->user->checkAccess('venderores')) {
            if ($_GET['id'] != Yii::app()->user->id) {
                $this->redirect(array('update', 'id' => Yii::app()->user->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'authitems' => $authitems,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('usuarios');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $tipo = 4;
        if (isset($_GET['tipo']))
            $tipo = $_GET['tipo'];

        if ($tipo == 4) {
            $model = usuarios::model()->findAll(array(
                "order" => "id desc",
            ));
        } else {
            $model = usuarios::model()->findAll(array(
                "condition" => "type =  $tipo",
                "order" => "id desc",
            ));
        }

        if (isset($_GET['usuarios']))
            $model->
                    attributes = $_GET['usuarios'];

        $this->render('admin', array('model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id']))
                $this->_model = usuarios::model()->findbyPk($_GET['id']);
            if ($this->_model === null)
                throw new
                CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuarios-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAssign($id, $item) {
        Yii::app()->authManager->assign($item, $id);
    }

    public function actionBuscar() {
        $key = $_POST["cliente"];
        $q = new CDbCriteria(array(
            'condition' => "razon_social LIKE :cliente",
            'params' => array(':cliente' => "%$key%")
        ));
        $clientes = usuarios::model()->findAll($q);
        foreach ($clientes as $cliente) {
            $country_name = str_replace($_POST['cliente'], '<b>' . $_POST['cliente'] . '</b>', $cliente ['razon_social']);
#echo '<li onclick="set_item(\'' . str_replace("'", "\'", $cliente['razon_social']) . '\')">' . $country_name . '</li>';
            echo '<li onclick="set_item(' . $cliente['id'] . ',\'' . $cliente ['razon_social'] . '\')">' . $country_name . '</li>';
        }
        Yii::app()->end();
    }

    public function actionEnviar() {
        $key = $_POST["email"];
        $clientes = usuarios::model()->find("email = '$key'");

        if ($clientes) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $password = substr(str_shuffle($chars), 0, 8);
            $clientes->password = $password;
            $clientes->save();
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: info@sifolimp.com\r\nReply-To: info@sifolimp.com";
            $res = mail($key, "Nueva password sifolimp", " Su nueva password sifolimp es: $password", $headers);
            if ($res == 1) {
                echo "Se ha enviado su nueva password al mail.";
            } else {
                echo "Ha fallado el envio de su nueva password, contactese con el administrador.";
            }
        } else {
            echo "No existe ese email";
        }
    }

    public function actionStatus() {
        $user_id = $_POST["user_id"];
        $status = $_POST["status"];

        $result = usuarios::model()->updateByPk($user_id, array(
            'status' => $status
        ));
        if ($result) {
            echo "OK";
        } else {
            echo "ERROR";
        }
    }

}
