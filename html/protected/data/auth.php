
<?php

#$model = usuarios::model()->findAllByAttributes(array('email' => Yii::app()->user->getId()));
#$rol = $model->rol;
$rol = Yii::app()->user->rol;
$auth = Yii::app()->authManager;

#$auth->createOperation('usuariosController', 'Usuarios Controller');
#$auth->createOperation('productosController', 'Productos Controller');
#$auth->createOperation('pedidosController', 'Pedidos Controller');

$role = $auth->createRole('admin');
#$role->addChild('usuariosController');
#$role->addChild('productosController');
#$role->addChild('pedidosController');

$role = $auth->createRole('clientes');
#$role->addChild('pedidosController');

$role = $auth->createRole('vendedores');
#$role->addChild('pedidosController');

switch ($rol) {
    case 1:
        #$bizRule = 'return !Yii::app()->user->isGuest;';
        #$auth->createRole('authenticated', 'authenticated user', $bizRule);
        $auth->createRole('admin', 'admin user', $rol);
        break;
    case 2:
        #$bizRule = 'return Yii::app()->user->isGuest;';
        $auth->createRole('clientes', 'clientes user', $rol);
        break;
    case 3:
        #$bizRule = 'return Yii::app()->user->isGuest;';
        $auth->createRole('vendedores', 'vendedores user', $rol);
        break;
}
$auth->save();
?>