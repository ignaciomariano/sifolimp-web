<?php

class productosTest extends WebTestCase
{
	public $fixtures=array(
		'productoses'=>'productos',
	);

	public function testShow()
	{
		$this->open('?r=productos/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=productos/create');
	}

	public function testUpdate()
	{
		$this->open('?r=productos/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=productos/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=productos/index');
	}

	public function testAdmin()
	{
		$this->open('?r=productos/admin');
	}
}
