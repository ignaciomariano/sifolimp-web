<?php

class pedidosTest extends WebTestCase
{
	public $fixtures=array(
		'pedidoses'=>'pedidos',
	);

	public function testShow()
	{
		$this->open('?r=pedidos/view&id=1');
	}

	public function testCreate()
	{
		$this->open('?r=pedidos/create');
	}

	public function testUpdate()
	{
		$this->open('?r=pedidos/update&id=1');
	}

	public function testDelete()
	{
		$this->open('?r=pedidos/view&id=1');
	}

	public function testList()
	{
		$this->open('?r=pedidos/index');
	}

	public function testAdmin()
	{
		$this->open('?r=pedidos/admin');
	}
}
