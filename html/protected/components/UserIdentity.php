<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    const ERROR_EMAIL_INACTIVE = 100;

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        #$user = usuarios::model()->find('LOWER(email)=?', array(strtolower($this->username)));

        $criteria = new CDbCriteria;
        $criteria->condition = 'email=:email';
        $criteria->params = array(':email' => $this->username);
        $user = usuarios::model()->find($criteria);
        #print_r($user);exit;

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            #print_r("ERROR_USERNAME_INVALID");exit;
        } elseif ($user->password != md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            #print_r("ERROR_PASSWORD_INVALID");exit;
        } elseif ($user->status != 1) {
            $this->errorCode = self::ERROR_EMAIL_INACTIVE;
            #print_r("USUARIO DESACTIVADO");exit;
        } else {
            $this->setState('email', $user->email);
            $this->setState('nombre', $user->nombre);
            $this->setState('apellido', $user->apellido);
            $this->_id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}
