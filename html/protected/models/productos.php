<?php

/**
 * This is the model class for table "productos".
 *
 * The followings are the available columns in table 'productos':
 * @property string $id
 * @property string $codigo
 * @property string $descripcion
 * @property string $marca
 * @property string $modelo
 * @property string $categoria
 * @property string $embalaje
 * @property string $nombre_foto
 * @property string $precio
 * @property string $status
 * 
 * The followings are the available model relations:
 * @property Pedidos[] $pedidoses
 */
class productos extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'productos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            #array('nombre_foto', 'file', 'allowEmpty' => false),
            #array('nombre_foto', 'file', 'types' => 'jpg,jpeg', 'maxSize' => 30720),
            #array('codigo, descripcion, marca, modelo, categoria, embalaje, nombre_foto', 'precio', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            // array('categoria', 'integerOnly' => true),
            #array('id, codigo, descripcion, marca, modelo, categoria, embalaje, nombre_foto', 'precio', 'safe', 'on' => 'search'),
            array('codigo, descripcion, marca, modelo, categoria_id,precio,nombre_foto', 'required',
                'on' => 'create',
                'message' => 'Por favor escriba un valor para {attribute}.'),
            array('codigo, descripcion, marca, modelo, categoria_id,precio', 'required',
                'on' => 'update',
                'message' => 'Por favor escriba un valor para {attribute}.'),
            array('precio,embalaje,status', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pedidos' => array(self::MANY_MANY, 'pedidos', 'pedidosproductos(producto_id,pedido_id,cant)'),
            'categoria' => array(self::BELONGS_TO, 'categorias', 'categoria_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'categoria_id' => 'Categoria',
            'embalaje' => 'Embalaje',
            'nombre_foto' => 'Foto',
            'precio' => 'Precio',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models 
     * based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('descripcion', $this->descripcion, true);
        $criteria->compare('marca', $this->marca, true);
        $criteria->compare('modelo', $this->modelo, true);
        $criteria->compare('categoria_id', $this->categoria_id, true);
        $criteria->compare('embalaje', $this->embalaje, true);
        $criteria->compare('nombre_foto', $this->nombre_foto, true);
        $criteria->compare('precio', $this->precio, true);
        $criteria->compare('status', $this->status, true);
        return new CActiveDataProvider('productos', array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * @return productos the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
