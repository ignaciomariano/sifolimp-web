<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property string $id
 * @property string $razon_social
 * @property string $cuit
 * @property string $password
 * @property string $nombre
 * @property string $apellido
 */
class usuarios extends CActiveRecord {

    public $old_password;
    public $new_password;
    public $repeat_password;
 
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'usuarios';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            #array('email, razon_social, cuit, password, nombre, apellido', 'type', 'length', 'max' => 45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('email, razon_social, nombre, apellido, cuit, password, type', 'required',
                'on' => 'create',
                'message' => 'Por favor escriba un valor para {attribute}.'),
            array('cuit', 'numerical', 'integerOnly'=>true, 'message' => 'Solo se permiten numeros.'),
            array('cuit', 'unique', 'message' => 'Este cuit ya esta siendo utilizado.'),
            array('email', 'unique', 'message' => 'Este email ya esta siendo utilizado.'),
            array('email, razon_social, nombre, apellido, cuit, type', 'required',
                'on' => 'update',
                'message' => 'Por favor escriba un valor para {attribute}.'),
            array('status', 'required',
                'on' => 'status'),
            array('id, password', 'safe'),

            #CHANGE PASSWORD
            array('old_password, new_password, repeat_password', 'required',
                     'on' => 'changepassword',
                     'message' => 'Por favor complete el campo.'
            ),
            array('old_password', 'findPasswords', 'on' => 'changepassword'),
            array('repeat_password', 'compare', 'compareAttribute'=>'new_password', 
                    'on'=>'changepassword',
                    'message' => 'Las contraseñas no son iguales.'
            ),
            #END CHANGE PASSWORD
        );
    }

    public function findPasswords($attribute, $params) {
        $user = usuarios::model()->findByPk(Yii::app()->user->id);
        if ($user->password != md5($this->old_password))
            $this->addError($attribute, 'La antigua password es incorrecta.');
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuarios' => array(self::HAS_MANY, 'pedidos', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'email' => 'Email',
            'razon_social' => 'Razon Social',
            'cuit' => 'Cuit',
            'password' => 'Password',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'type' => 'tipo de usuario',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('cuit', $this->cuit, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('apellido', $this->apellido, true);
        $criteria->compare('type', $this->type, true);

        return new CActiveDataProvider('usuarios', array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * @return usuarios the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    #public function beforeSave() {
        #var_dump("nachito");exit;
        #$pass = md5($this->password);
        #$this->password = $pass;
        #return true;
    #}

}
