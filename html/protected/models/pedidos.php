<?php

/**
 * This is the model class for table "pedidos".
 *
 * The followings are the available columns in table 'pedidos':
 * @property string $id
 * @property string $user_id
 * @property string $date
 * @property string $cat
 */
class pedidos extends CActiveRecord {

    #public $razon_social;
    #public $nombre;
    #public $apellido;
    #public $cant;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pedidos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id', 'required'),
            array('user_id', 'length', 'max' => 11),
            array('date', 'length', 'max' => 45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, date, nombre, apellido, razon_social', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(            
            'productos' => array(self::MANY_MANY, 'productos', 'pedidosproductos(pedido_id,producto_id)'),
            'usuarios' => array(self::BELONGS_TO, 'usuarios', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'user_id' => 'User',
            'date' => 'Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.        
        $criteria = new CDbCriteria;
        $criteria->with = array("usuarios");
        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('razon_social', $this->razon_social, true);
        $criteria->compare('apellido', $this->apellido, true);
        $criteria->compare('nombre', $this->nombre, true);
        return new CActiveDataProvider('pedidos', array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * @return pedidos the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
