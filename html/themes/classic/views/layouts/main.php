<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="en">

        <!-- blueprint CSS framework -->
        <!--
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl; ?>/css/screen.css"
        media="screen, projection">
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
        -->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl; ?>/css/ie.css" 
        media="screen, projection">
        <![endif]-->

        <!--
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl; ?>/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php #echo Yii::app()->request->baseUrl; ?>/css/form.css">
        -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
        <!--
        <script src="<?php #echo Yii::app()->request->baseUrl; ?>/js/jquery.floatThead.js"></script>
        <script src="<?php #echo Yii::app()->request->baseUrl; ?>/js/jquery.floatThead-slim.js"></script>        
        -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" >
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <!--<div class="container" id="page">-->
        <div id="header">
            <div id="logo"><?php #echo CHtml::encode(Yii::app()->name);?></div>
        </div>

        <?php if (!Yii::app()->user->isGuest) { ?>
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="http://www.sifolimp.com.ar/" title="Sifolimp.com.ar">
                            <img alt="Brand" src="/images/sifolimp.jpg" alt="Sifolimp" />
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <!--array('label' => 'Home', 'url' => array('/site/index'), 'visible' => !Yii::app()->user->isGuest),-->
                        <?php if (Yii::app()->user->checkAccess('clientes')) { ?>
                            <li class="active"><a href="/site/index">Home</a></li>
                            <li><a href="/pedidos/mispedidos/">Mis pedidos</a></li>
                            <li><a href="/usuarios/update/<?php echo Yii::app()->user->id; ?>">Perfil</a></li>
                            <li><a href="/site/logout">Logout (<?php echo Yii::app()->user->email; ?>)</a></li>
                        <?php } else { ?>
                            <?php if (!Yii::app()->user->isGuest) { ?>
                                <li class="active">
                                    <a href="/site/index">Home</a>
                                    <!--<a href="home-vendedor.html">Home</a>-->
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pedidos <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!--array('label' => 'Pedidos', 'url' => array('/pedidos/index'), 'visible' => !Yii::app()->user->isGuest),-->
                                        <li><a href="/pedidos/admin">lista de pedidos<i class="fa fa-bars"></i></a></li>
                                        <!--<li><a href="/pedidos/index">lista de pedidos<i class="fa fa-bars"></i></a></li>-->
                                        <li><a href="/pedidos/create">crear pedidos<i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Productos <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!--array('label' => 'Productos', 'url' => array('/productos/index'), 'visible' => !Yii::app()->user->isGuest),-->
                                        <li><a href="/productos/admin">lista de productos<i class="fa fa-bars"></i></a></li>
                                        <li><a href="/productos/create">crear productos<i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Clientes <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <!--array('label' => 'Usuarios', 'url' => array('/usuarios/index'), 'visible' => !Yii::app()->user->isGuest),-->
                                        <!--<li><a href="/usuarios/index">lista de clientes<i class="fa fa-bars"></i></a></li>-->
                                        <li><a href="/usuarios/admin">lista de clientes<i class="fa fa-bars"></i></a></li>
                                        <li><a href="/usuarios/create">crear clientes<i class="fa fa-plus"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="/site/logout">Logout (<?php echo Yii::app()->user->email; ?>)</a></li>
                            <?php } elseif (Yii::app()->user->isGuest) { ?>
                                <li><a href="/site/login">Login</a></li>
                            <?php } ?>
                        <?php } ?>
                        <!--
                        array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Logout (' . Yii::app()->user->name . /* Yii::app()->user->rol . */')',
                        'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        -->
                    </ul>
                </div>
            </nav><!-- /. nav-->
        <?php } ?>

        <?php if (isset($this->breadcrumbs)): ?>
            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
            ));
            ?><!-- breadcrumbs -->
        <?php endif ?>

        <?php echo $content; ?>

        <div class="clear"></div>

        <?php if (!Yii::app()->user->isGuest) { ?>
            <div id="footer">
                <div class="footer container-fluid">
                    <div class="container">
                        ©2013 Sifolimp
                    </div>
                </div>
            </div><!-- footer -->
        <?php } ?>

        <!--</div>-->
        <!-- page -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/wow.min.js"></script>

    </body>
</html>
